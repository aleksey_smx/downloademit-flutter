import 'dart:async';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  bool _loading;
  double _progressValue;

  @override
  void initState() {
    _loading = false;
    _progressValue = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.indigo,
        appBar: AppBar(
          title: Text("Test Application"),
          centerTitle: true,
        ),
        body: Container(
           height: double.infinity,
           width: double.infinity,
           color: Colors.red,
           padding: EdgeInsets.all(16),
              
          child: Center(
             
              child: _loading
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        LinearProgressIndicator(value: _progressValue),
                        Text(
                          "${((_progressValue) * 100).round()}%",
                          style: TextStyle(color: Colors.white, fontSize: 33),
                        ),
                      ],
                    )
                  : Text(
                      "Press button to download",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    )),
        ),
        floatingActionButton: FloatingActionButton(
          focusColor: Colors.pink,
          child: Icon(Icons.cloud_download),
          onPressed: () {
            setState(() {
              if (!_loading) {
                _loading = !_loading;
                _updateProgress();
              }
            });
          },
        ),
      ),
    );
  }

  void _updateProgress() {
    const oneSec = const Duration(milliseconds: 50);
    Timer.periodic(oneSec, (Timer t) {
      setState(() {
        _progressValue += 0.0135;

        if (_progressValue >= 1.0) {
          _loading = false;
          _progressValue = 0.0;
          t.cancel();
          return;
        }
      });
    });
  }
}
